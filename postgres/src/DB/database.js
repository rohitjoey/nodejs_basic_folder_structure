const { Pool } = require("pg");
const { dbConnectionParams } = require("../config");

const pool = new Pool(dbConnectionParams);

module.exports = {
  query: (text, params) => pool.query(text, params),
};
