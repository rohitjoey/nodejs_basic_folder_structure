const database = require("../DB/database");
const updateTaskQueryGenerate = require("../util/update-query");

class TaskService {
  getTask = async (id) => {
    const { rows } = await database.query("SELECT * FROM task WHERE id=$1", [
      id,
    ]);
    // console.log(rows[0]);
    // console.log(rows);
    if (rows.length < 1) {
      return { status: 400, msg: `No task with id : ${id}` };
    }
    return { status: 200, task: rows[0] };
  };

  deleteTask = async (id) => {
    const result = await database.query("DELETE FROM task WHERE id=$1", [id]);
    // console.log(result);
    if (!result.rowCount || !result) {
      return { status: 400, msg: `No task with id : ${id}` };
    }
    return { status: 200, msg: "Task deleted successfully" };
  };

  // UPDATE task SET name='pitai' WHERE id='04310e57-0c61-48c5-8afc-c173d217fdac';

  updateTask = async (id, newDataObject) => {
    // console.log(id, newDataObject);
    const { query, values } = updateTaskQueryGenerate(id, newDataObject);
    console.log(query);
    const result = await database.query(query, values);
    // console.log(task);

    if (!result.rowCount || !result) {
      return { status: 400, msg: `No task with id : ${id}` };
    }
    return { status: 200, msg: "Task updated successfully" };
  };
}

module.exports = TaskService;
