const express = require("express");
const { taskRouter, homeRouter } = require("./routes");
const app = express();
const notFound = require("./middleware/not-found");
const errorHandler = require("./middleware/error-handle");
const { serverPort } = require("./config");

app.use(express.json());

app.use("/", homeRouter);

app.use("/api/v1/tasks", taskRouter);

app.use(notFound);
app.use(errorHandler);

app.listen(serverPort, () => {
  console.log(`Listening at port ${serverPort}`);
});
