const homeRouter = require("./home.router");
const taskRouter = require("./task.router");

module.exports = { homeRouter, taskRouter };
