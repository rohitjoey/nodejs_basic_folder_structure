const database = require("../DB/database");
const TaskService = require("../services/task.service");

const getAlltasks = async (req, res) => {
  const task = await database.query("SELECT * FROM task");
  res.status(200).json(task.rows);
  // console.log(result.rows);
};

const createTask = async (req, res) => {
  const { name, completed } = req.body;
  // console.log(name, completed);
  let queryString;
  let queryParams;
  if (typeof completed !== "undefined") {
    queryString = "INSERT INTO task(name,completed) VALUES ($1,$2) RETURNING *";
    queryParams = [name.trim(), completed];
  } else {
    queryString = "INSERT INTO task(name) VALUES ($1) RETURNING *";
    queryParams = [name.trim()];
  }

  // console.log(queryString);
  // console.log(queryParams);
  const result = await database.query(queryString, queryParams);
  //INSERT INTO task(name,completed) VALUES ('',);
  const data = result.rows[0];
  res.status(200).json({ data, msg: "Task created Successfully" });
};

const getTask = async (req, res) => {
  const { id: taskId } = req.params;
  const { status, task, msg } = await new TaskService().getTask(taskId);
  // console.log(result);
  res.status(status).json({ task, msg });
};

const deleteTask = async (req, res) => {
  const { id: taskId } = req.params;
  const { status, msg } = await new TaskService().deleteTask(taskId);

  res.status(status).json(msg);
};

const updateTask = async (req, res) => {
  const { id: taskId } = req.params;

  const { status, msg, task } = await new TaskService().updateTask(
    taskId,
    req.body
  );

  res.status(status).json({ task, msg });
};

module.exports = {
  getAlltasks,
  createTask,
  deleteTask,
  getTask,
  updateTask,
};
