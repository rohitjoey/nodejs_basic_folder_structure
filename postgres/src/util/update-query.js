const updateTaskQueryGenerate = (id, newDataObject) => {
  let query = ["UPDATE task"];
  query.push("SET");

  let set = [];
  let values = [];
  Object.keys(newDataObject).forEach((key, i) => {
    set.push(key + "=$" + (i + 1));
    values.push(newDataObject[key]);
  });
  // var val = obj[keys[i]];

  // Object.values(newDataObject).forEach((value) => {
  //   values.push(value);
  // });
  values = values.map((value) => {
    if (typeof value === "string") {
      return value.trim();
    }
    return value;
  });
  query.push(set.join(", "));

  query.push("WHERE id=" + "'" + id + "'");
  query = query.join(" ");

  return { query, values };
};

module.exports = updateTaskQueryGenerate;
