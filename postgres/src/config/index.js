const dotenv = require("dotenv");

dotenv.config();

const { dbConnectionParams } = require("./postgres.config");
const { serverPort } = require("./server.config");

module.exports = {
  dbConnectionParams,
  serverPort,
};
