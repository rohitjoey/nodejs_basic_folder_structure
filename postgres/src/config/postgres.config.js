// module.exports = {
//   databaseURL: process.env.MONGO_URI,
// };

module.exports.dbConnectionParams = {
  user: process.env.user,
  host: process.env.host,
  database: process.env.database,
  password: process.env.password,
  port: process.env.port,
};
