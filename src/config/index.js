const dotenv = require("dotenv");

dotenv.config();

const { databaseURL } = require("./db");
const { port } = require("./server");

module.exports = {
  databaseURL,
  port,
};
