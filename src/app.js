const express = require("express");
const { taskRouter, homeRouter } = require("./routes");
const app = express();
const notFound = require("./middleware/not-found");
const errorHandler = require("./middleware/error-handle");
const { port, databaseURL } = require("./config");
const connectDB = require("./DB/connectdb");

app.use(express.json());

app.use("/", homeRouter);

app.use("/api/v1/tasks", taskRouter);

app.use(notFound);
app.use(errorHandler);

const start = async () => {
  try {
    await connectDB(databaseURL).then(() => {
      console.log("Database Connected");
    });

    app.listen(port, () => {
      console.log(`Listening at port ${port}`);
    });
  } catch (error) {
    console.log(error);
  }
};

start();
