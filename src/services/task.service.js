const Task = require("../models/task");

class TaskService {
  getTask = async (id) => {
    const task = await Task.findOne({ _id: id });
    if (!task) {
      return { status: 400, msg: `No task with id : ${id}` };
    }
    return { status: 200, task };
  };

  deleteTask = async (id) => {
    const task = await Task.findOneAndDelete({ _id: id });
    if (!task) {
      return { status: 400, msg: `No task with id : ${id}` };
    }
    return { status: 200, msg: "Task deleted successfully" };
  };

  updateTask = async (id, newData) => {
    const task = await Task.findByIdAndUpdate({ _id: id }, newData, {
      new: true,
      runValidators: true,
    });

    if (!task) {
      return { status: 400, msg: `No task with id : ${id}` };
    }
    return { status: 200, task };
  };
}

module.exports = TaskService;
