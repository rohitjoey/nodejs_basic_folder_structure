const Task = require("../models/task");
const TaskService = require("../services/task.service");

const getAlltasks = async (req, res) => {
  const task = await Task.find({});

  res.status(200).json({ task });
};

const createTask = async (req, res) => {
  const task = await Task.create(req.body);
  res.status(201).json({ task });
};

const getTask = async (req, res) => {
  const { id: taskId } = req.params;
  const { status, msg, task } = await new TaskService().getTask(taskId);

  res.status(status).json({ task, msg });
};

const deleteTask = async (req, res) => {
  const { id: taskId } = req.params;
  const { status, msg } = await new TaskService().deleteTask(taskId);

  res.status(status).json(msg);
};

const updateTask = async (req, res) => {
  const { id: taskId } = req.params;

  const { status, msg, task } = await new TaskService().updateTask(
    taskId,
    req.body
  );

  res.status(status).json({ task, msg });
};

module.exports = {
  getAlltasks,
  createTask,
  deleteTask,
  getTask,
  updateTask,
};
