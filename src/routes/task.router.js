const express = require("express");
const asyncWrapper = require("../middleware/async-wrapper");

const {
  updateTask,
  getTask,
  deleteTask,
  createTask,
  getAlltasks,
} = require("../controllers/task.controller");

//user route
const router = express.Router();

router.route("/").get(asyncWrapper(getAlltasks)).post(asyncWrapper(createTask));
router
  .route("/:id")
  .get(asyncWrapper(getTask))
  .patch(asyncWrapper(updateTask))
  .delete(asyncWrapper(deleteTask));

module.exports = router;
